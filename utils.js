const find = require("find");
const fs = require("fs");
const path = require("path");

/** @description Gets the content from a file into a text string */
module.exports.getTextFromFile = function(filePath = ""){
    return fs.readFileSync(filePath, {
        encoding: 'UTF-8'
    }).toString();
}

/** @description Gets the content from a .json file into an object */
module.exports.getObjectFromJson = function(filePath = ""){
    return JSON.parse(fs.readFileSync(filePath, {
        encoding: 'UTF-8'
    }));
}

/**
 * @description Gets the HBS files from a site
 * @param {*} sitePath The root path for the site's handlebar files
 */
module.exports.getHbs = function(sitePath=""){

    const hbs = find.fileSync(/\.hbs$/, sitePath);
    let _readHbs = {
        'fileName':"content"
    }
    _readHbs = {};

    for(let i=0;i<hbs.length;i++){
        _readHbs[hbs[i]] = module.exports.getTextFromFile(hbs[i]);
    }

    return _readHbs;
}


/**
 * @description Gets the translations from a site
 * @param {*} sitePath The path from where the translations will be loaded
 */
module.exports.getTranslations = function(sitePath=""){
    let translations = find.fileSync(/\.json$/, path.join(sitePath, 'translations'));
    let _readTranslations = {
        "filename":{"key":"string"}
    }
    
    _readTranslations = {};
    for(let i=0;i<translations.length;i++){
        _readTranslations[translations[i]] = module.exports.getObjectFromJson(translations[i]);
    }
    return _readTranslations;
}