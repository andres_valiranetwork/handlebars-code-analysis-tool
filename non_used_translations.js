const  html_parser = require('node-html-parser');
const fs = require("fs");
const utils = require("./utils");
const path = require("path");
const wordMatchingRegExp = new RegExp(/([\w]{1,})((\s){1,})?([\w]{1,})/gi);

/**
 * @description Analizes a text and matches all occurrences of a text
 */
function getTextOccurrences(text = "", regexp = wordMatchingRegExp){
    let match;
    let matches = [{
        match:"",
        start:0,
        end:0
    }];
    matches = [];
    while ((match = regexp.exec(text)) !== null) {
        matches.push({
            match: match[0],
            start: match.index,
            end: regexp.lastIndex
        });
    }
    return matches.length > 0 ? matches : null;
}

/**
 * @description Gets the possibly untranslated words from a piece of file
 * @param fileContents  The raw text content from the .HBS file
 */
function getWords(fileContents = ""){
    fileContents = fileContents
    //We remove the handlebars logic
    .replace(/(\{\{)(\s)?[!\-\-]?[>]?[a-z0-9=_\-"'. ()#/]{1,}(\})(\s)?(\})/gi, '')
    //We replace the handlebars with sub-"{}", for example, {{replace translations.serie_watch "{title}" serie.title}}
    .replace(/(\{\{)(\s)?[!\-\-]?[>]?[a-z0-9=_\-"'. (){}#/]{1,}(\})(\s)?(\})/gi, '')
    //We replace all the other occurrences inside handlebar brackets
    .replace(/(\{){1,}(.*)(\}){1,}/gi, '')
    //We remove the &nbp;, &lkka; &etc. logic
    .replace(/(\&)[\w]{1,}(\;)/g, '')
    //We remove the new lines
    .replace(/\\n/g, '');
        
    
    //Generates a DOM Object for parsing the text
    let DOMObject = html_parser.parse(fileContents);
    //Gets the raw text
    let treatedText = DOMObject.rawText;
    //We remove the spaces
    treatedText = treatedText
    .replace(/[\n]/g, '')
    .replace(/[\s]{2,}/g, '');
    //We get the text ocurrences
    let ocurrences = getTextOccurrences(treatedText, wordMatchingRegExp);
    if(ocurrences){
        return {
            ocurrences,
            treatedText
        }
    }
    return null;
}

/**
 * @param {*} filePath The file path for the .HBS
 */
function getFileWords(filePath="/home/azolot/Documents/Important/_work/NamasteCorp/Testing/translations/data/testing/searching_for_missing_text.hbs"){
    let fileContents = fs.readFileSync(filePath, {
        encoding: 'UTF-8'
    }).toString();
    return getWords(fileContents);
}

/**
 * @description We run the data
 */
function analizeSite(sitePath = "/home/azolot/Documents/Important/_work/NamasteCorp/Testing/translations/data/testing/"){
    let result = {};
    
    //Gets all the HBS file contents and paths
    result.hbs = utils.getHbs(sitePath);
    for(let key of Object.keys(result.hbs)){
        let words = getWords(result.hbs[key]);
    
        if(words){
            console.log(`\n\nThe file ${key} has possibly untranslated words`);
            console.log("Matches: "+words.ocurrences.map((ocurrence)=>ocurrence.match).join("<--\n"));
            console.log("Related Text: "+words.treatedText);
        }
    }
}


/**
 * @description Analize sites and outputs the analysis result
 */
function analizeSites(){
    //Folder Sites that are not meant to be analized
    const foldersToIgnore = ["common"];
    //The root path of all the sites
    const rootPath = "/home/azolot/Documents/Important/_work/NamasteCorp/movies-websites/src/";
    //The folder sites, already filtered
    let folderSites = fs.readdirSync(rootPath).filter((site)=>{
        return foldersToIgnore.indexOf(site)<0;
    });
    //folderSites = ['filmstarts.film'];
    //For each site folder
    for(const siteFolder of folderSites){
        //Analize and print results
        console.log("\n\n\n\n++++++++++ Analizing: ",siteFolder+" ++++++++++");
        let jointPath = path.join(rootPath, siteFolder);
        analizeSite(jointPath);
    }
}

analizeSites();