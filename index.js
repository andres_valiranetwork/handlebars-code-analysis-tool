const fs = require("fs");
const path = require("path");
const utils = require("./utils");
const ftcDefaultReg = new RegExp('translations.[a-z-_0-9]*','g');  //File translation call default regExp

/**
 * @description Gets the unmatching translations among local files
 * @param {*} translations The translations object
 */
function getMissingTranslationKeys(translations={ "filename":{"key":"string"} }){
    let translationFiles = Object.keys(translations);
    let keys = {
        total:{
            "key":  //The translation key on the .json
                [
                    "translationFileName"   //The translation filename
                ]
        },
        missing:{
            "key":  //The missing // missmatching key on the jsons
                [
                    'translationFileName'   //The json where it's missing
                ]
        }
    };
    keys = {
        total:{},
        missing:{}
    };
    //For each translation filename
    for(const filename of translationFiles){
        //For each key in the translation objects
        for(const key in translations[filename]){
            //If the array for files of that key has not been initialized
            if(!keys.total[key]){
                //Initialize array and append key
                keys.total[key] = [];
                keys.total[key] = [filename];
            }else{
                //Push key into array
                keys.total[key].push(filename);
            }
        }
    }
    //For each key found
    for(const key in keys.total){
        //If the ammount of files in which those are found are not equal to the ammount
        //of translation files, it means they're missing in one file. Find which file
        if(keys.total[key].length < translationFiles.length){
            //For each translation file
            for(const file of translationFiles){
                //If the file is not included in the translations keys array
                if(keys.total[key].indexOf(file) < 0 ){
                    //If the key hasn't been added
                    keys.missing[key] = keys.missing[key] ? keys.missing[key] : [];
                    //Add it to missing files
                    keys.missing[key].push(file);
                }
            }
        }
    }
    return keys;
}

/**
 * @description Analizes a text and matches all occurrences of a text
 */
function getTextOccurrences(text = "", regexp = ftcDefaultReg){
    let match;
    let matches = [{
        match:"",
        start:0,
        end:0
    }];
    matches = [];
    while ((match = regexp.exec(text)) !== null) {
        matches.push({
            match: match[0],
            start: match.index,
            end: regexp.lastIndex
        });
        //console.log(`Encontrado ${match[0]} inicio=${match.index} final=${regexp.lastIndex}.`);
    }
    return matches.length > 0 ? matches : null;
}

/**
 * @description Gets the called translations for each key for a certain file
 * @param {*} text The text of the HBS file
 */
function getCalledTranslations(text){
    return getTextOccurrences(text);
}

/**
 * @description Gets the used translations across the HBS files from a site
 * @param {*} hbs The HBS hash table of files found
 */
function getUsedTranslations(hbs = {key:"string"}){
    let translationsUsed = {
        "key_of_file": ["fileWhereItsUsed"]
    };
    translationsUsed = {};
    let hbsFiles = Object.keys(hbs);
    //For each HBS file
    for(const filePath of hbsFiles){
        //Get matches of that file
        let thisMatches = getCalledTranslations(hbs[filePath]);
        //If this file has matches
        if(thisMatches){
            //For each match on the matches
            for(const match of thisMatches){
                //Get translation key
                let key = match.match.split(".").pop();
                //If key index hasn't been initialized, initialize it
                translationsUsed[key] = translationsUsed[key] ? translationsUsed[key] : [];
                //Push into the translations-used hash table
                translationsUsed[key].push(filePath);
            }
            //break;
        }
    }
    return translationsUsed;
}

/**
 * @description Graphycally Prints the given data
 * @param {*} array The array of keys to print
 * @param {*} text The title for the print
 */
function printArray(array = [""], text = "PLACEHOLDER TEXT"){
    console.log(`*************** ++ ${text}***************`);
    console.log(`--`+array.join(`\n--`));
    console.log(`*************** -- ${text}***************`);
}

/**
 * @description Checks wether the site has a "translations" folder
 * @param {*} sitePath The root path for the site's files
 */
function hasTranslationsFolder(sitePath = ""){
    return fs.existsSync(path.join(sitePath, 'translations'));
}

/**
 * @description Analizes a given site looking for keys and returns findings
 * @param {*} sitePath The path to the given site
 */
function analizeSite(sitePath = "/home/azolot/Documents/Important/_work/NamasteCorp/Testing/translations/data/src/cinecalidad.plus"){
    //The resulting data from the site's analysis
    let result = {};
    
    //Gets all the HBS file contents and paths
    result.hbs = utils.getHbs(sitePath);
    //Gets the translations objects used
    result.translations = utils.getTranslations(sitePath);

    //Translation keys missmatching between .json files and total keys used
    result.missing = getMissingTranslationKeys(result.translations);
    
    //Translation keys found in the total array
    result.totalTranslationKeys = Object.keys(result.missing.total);

    result.missingTranslationKeys = Object.keys(result.missing.missing);

    result.usedTranslations = getUsedTranslations(result.hbs);
    result.usedTranslationKeys = Object.keys(result.usedTranslations);    //Actively-used translation keys on the templates

    //Translations missing (found in .json but not found in templates)
    result.nonUsedTranslations = [""];
    result.nonUsedTranslations = [];

    for(const translationKey of result.totalTranslationKeys){
        if(result.usedTranslationKeys.indexOf(translationKey)<0){
            result.nonUsedTranslations.push(translationKey);
        }
    }



    result.possibleMissingKeys = [];                         //The keys that are not referenced in any .json file and are possibly missing
    for(const translationKey of result.usedTranslationKeys){
        if(result.totalTranslationKeys.indexOf(translationKey)<0){
            result.possibleMissingKeys.push(translationKey);
        }
    }
    
    return result;
}

/**
 * @description Analize sites and outputs the analysis result
 */
function analizeSites(){
    //Folder Sites that are not meant to be analized
    const foldersToIgnore = ["common"];
    //The root path of all the sites
    const rootPath = "/home/azolot/Documents/Important/_work/NamasteCorp/movies-websites/src/";
    //The folder sites, already filtered
    let folderSites = fs.readdirSync(rootPath).filter((site)=>{
        return foldersToIgnore.indexOf(site)<0;
    });
    //folderSites = ['filmstarts.film'];
    //For each site folder
    for(const siteFolder of folderSites){
        //Analize and print results
        console.log("\n\n\n\n++++++++++ Analizing: ",siteFolder+" ++++++++++");
        let jointPath = path.join(rootPath, siteFolder);
        const hasTranslations = hasTranslationsFolder(jointPath);
        if(hasTranslations){
            const siteResult = analizeSite(jointPath);
            console.log(`\n\n***** -- ${siteFolder} --`);
            printArray(siteResult.missingTranslationKeys, "MISSING TRANSLATION KEYS BETWEEN FILES");
            printArray(siteResult.usedTranslationKeys, "USED TRANSLATION KEYS");
            printArray(siteResult.nonUsedTranslations, 'NON-USED TRANSLATION KEYS');
            printArray(siteResult.possibleMissingKeys, 'POSSIBLY-MISSING TRANSLATION KEYS');
        }else{
            console.log(`\n\n ATTENTION: ${siteFolder} doesn't have a translations folder.`);
        }
    }
}

analizeSites();